package com.bogledesigns.theoremProver;

import java.util.ArrayList;
import java.util.List;

/*
 * @author: David Bogle
 * 
 * The purpose of this class is to create an object that
 * represents a sentence in Conjunctive Normal Form.
 * The sentence is composed of clauses that are conjoined.
 * 
 * This class also holds the public final characters that are
 * used by other classes to signify conjunctions, disjunctions,
 * and negations.
 */

public class CNFSentence {
	
	//=== Instance Variables =================================//
	
	private List<CNFClause> clauses;
	
	//constants
	public static final char AND = '&';
	public static final char OR  = '|';
	public static final char NOT = '\'';// just an apostrophe
	
	//=== Constructors =======================================//
	
	public CNFSentence( String sentence ) {
		//build the clauses
		this.clauses = clausesFromString( sentence.trim().toLowerCase() );
	}
	
	public CNFSentence( List<CNFClause> clauses ) {
		this.clauses = clauses;
	}
	
	//=== Observors / Accessors ==============================//
	
	public List<CNFClause> getClauses() {
		return this.clauses;
	}
	
	public String toString() {
		return stringFromClauses( this.clauses );
	}
	
	public CNFSentence negation() {
		List<CNFClause> newList = new ArrayList<CNFClause>();
		CNFNegation( 0, new CNFClause(""), newList );
		return new CNFSentence( newList );
	}
	
	//=== Helper Methods =====================================//
	
	//this recursive method is called by the negation() method
	private void CNFNegation(int iteration, CNFClause clausePassedIn,
			List<CNFClause> list) {

		for (int i = 0; i < this.clauses.get(iteration).getLiterals().size(); i++) {
			CNFClause newClause = new CNFClause(clausePassedIn.toString());
			newClause.add(clauses.get(iteration).getLiterals().get(i).negation());
			if (iteration < this.clauses.size() - 1)
				CNFNegation(iteration + 1, newClause, list);
			else
				list.add(newClause);
		}
	}
	
	//used in the constructor to get clauses from a string
	private List<CNFClause> clausesFromString( String sentence ) {
		List<CNFClause> clauses = new ArrayList<CNFClause>();
		
		String[] parts = sentence.split( ""+CNFSentence.AND );
		
		for( int i=0; i<parts.length; i++ ) {
			clauses.add( new CNFClause( parts[i] ));
		}
		
		return clauses;
	}
	
	//used by toString()
	private String stringFromClauses( List<CNFClause> clauses ) {
		String returnString = "";
		
		for( int i=0; i<clauses.size(); i++ ) {
			returnString += clauses.get(i);
			returnString += (i+1<clauses.size()) ? " " + AND + " " : "";
		}
		
		return returnString;
	}
}