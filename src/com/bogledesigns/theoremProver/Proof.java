package com.bogledesigns.theoremProver;

import java.util.List;

/*
 * @author: David Bogle
 * 
 * The purpose of this class is to create an object that
 * represents a proof that uses the resolution rule and proof
 * by contradiction to show weather or not a given theorem is
 * provable with a set of axioms.  All axioms and the theorem
 * are assumed to be in Conjunctive Normal Form.
 */

public class Proof {

	//=== Instance Variables =================================//
	
	private List<CNFClause> axioms;
	private CNFSentence  theorem;
	private String       instruction;
	private int          nextStep;
	private Boolean      provable;
	//Note: the Boolean class is used instead of the primitive
	//type because provable needs to have a null value until it
	//is either shown to be true or false
	
	//=== Constructors =======================================//
	
	public Proof( List<CNFClause> axioms, CNFSentence theorem ) {
		this.axioms      = axioms;
		this.theorem     = theorem;
		this.instruction = "";
		this.nextStep    = 1;
		this.provable    = null;
	}
	
	//=== Observers / Accessors ==============================//
	
	public CNFSentence getTheorem() {
		return this.theorem;
	}
	
	public List<CNFClause> getAxioms() {
		return this.axioms;
	}
	
	public Boolean isProvable() {
		return provable;
	}
	
	public String getInstruction() {
		return this.instruction;
	}
	
	//=== Transformers / Mutators ============================//
	
	// this method is used to walk through the process of proving
	// the theorem true or false
	public void nextStep() {
		switch (this.nextStep) {
		case 1:
			step1();
			break;
		case 2:
			step2();
			break;
		case 3:
			step3();
			break;
		case 4:
			step4();
			break;
		}
	}

	public void solve() {
		//as long as the theorem has not been proven true or unprovable...
		while( this.provable == null ) {
			//step through the proof
			this.nextStep();
		}
	}
	
	//=== Helper Methods =====================================//
	
	private void step1() {
		//state the axioms and postulated theorem
		this.instruction = "The given axioms are";
		
		for( CNFClause axiom: axioms )
			instruction += "\n" + axiom;
		
		this.instruction += "\nThe postulated theorem is " + theorem + ".\n";
		this.nextStep = 2;
	}
	
	private void step2() {
		//negate the theorem
		this.theorem = this.theorem.negation();
		this.instruction = "The negated theorem is " + theorem + ".\n";
		this.nextStep = 3;
	}
	
	private void step3() {
		//add theorem to the axioms
		this.instruction = "Add the theorem to the list of knowns (axioms)."
			             + "\nThe theorem is added to the axioms as the following clause(s):";
		
		for( CNFClause clause : theorem.getClauses() )
			this.instruction += "\n" + clause;

		this.axioms.addAll( theorem.getClauses() );
		this.nextStep = 4;
	}
	
	//this step is a combination of the remaining steps from the assignment PDF
	private void step4() {
		instruction = "";
		//find two axioms that can resolve
		for( int i=0; i<axioms.size(); i++ ) {
			for( int j=i+1; j<axioms.size(); j++ ) {
				if( axioms.get(i).resolvesWith(axioms.get(j)) ) {
					List<CNFClause> resolvents = axioms.get(i).resolvents(axioms.get(j));
					for( CNFClause resolvent: resolvents ) {
						//if this resolvent is a new axiom...
						if( !axioms.contains( resolvent ) ) {
							instruction += "\n" + axioms.get(i) + " and " + axioms.get(j)
							            +  " are resolvent, and produce the following axiom: " + resolvent;
							axioms.add( resolvent );
							System.out.println( resolvent ); //=================================
							if( resolvent.toString().length() > 20 ) System.exit(0); //=========
							nextStep = 4;
						}
						//if the resolvent is empty, we have a contradiction
						if( resolvent.toString().equals("") ) {
							instruction += "\nThis is an empty disjunction, so we have found a contradiction,"
								        +  " and our theorem is true.";
							provable = new Boolean(true);
							break;//all done, end the loop
						}
					}//end for( Clause resolvent...
				}//end if axioms.get(i)...
				if( provable != null ) break;//if done, no need to loop
			}//end for j
			if( provable != null ) break;//if done, no need to loop
		}//end for i
		//if we did not resolve any new axioms...
		if( instruction.equals("") ) {
			instruction = "\nThere are no new axioms to be resolved, and we have found "
				        + "no contradictions, so our theorem cannot be proven true.";
			provable = new Boolean(false);
		}
	}
}