package com.bogledesigns.theoremProver;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/*
 * @author: David Bogle
 */

@SuppressWarnings("serial")
public class ProofGUI extends JFrame implements ActionListener {

	//=== Instance Variables =================================//
	
	private String       title       = "Resolution Theorem Prover v1.0";
	private int          width       = 640;
	private int          height      = 480;
	private Font         defaultFont = new Font("arial", Font.PLAIN, 18);
	private Color        lightRed    = new Color( 255, 220, 220 );
	private Color        lightGreen  = new Color( 220, 255, 220 );
	private Proof        proof;
	private List<CNFClause> axioms;
	
	//visual components
	private JScrollPane scrollPane;
	
	private JPanel topPanel;
	private JPanel firstRow;
	private JPanel secondRow;
	private JPanel thirdRow;
	private JPanel leftSide;
	private JPanel rightSide;
	
	private JButton addAxiomButton;
	private JButton removeAxiomButton;
	private JButton proveButton;
	private JButton resetButton;
	
	private JLabel axiomLabel;
	private JLabel axiomsLabel;
	private JLabel theoremLabel;
	private JLabel bump5px;
	
	private JTextArea instructions;
	private JTextArea displayText;
	
	private JTextField axiomTextField;
	private JTextField theoremTextField;
	
	private JComboBox axiomComboBox;
	
	private GridBagConstraints c;
	
	//=== Constructors =======================================//
	
	public ProofGUI(){
		proof  = null;
		axioms = new ArrayList<CNFClause>();
		buildWindow();
		centerOnScreen();
		buildVisualComponents();
	}
	
	//=== Helper Methods =====================================//
	
	private void buildWindow() {
		this.setSize( width, height );
		this.setTitle( title );
		this.setLayout( new GridBagLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void centerOnScreen() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int x = ( dim.width - this.width ) / 2;
		int y = ( dim.height - this.height ) / 2;
		this.setLocation( x, y );
	}
	
	private void buildVisualComponents() {
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		
		topPanel = new JPanel( new FlowLayout(FlowLayout.LEFT));
		this.add( topPanel, c );
		
		leftSide = new JPanel( new FlowLayout(FlowLayout.LEFT));
		leftSide.setPreferredSize( new Dimension(410, 130) );
		topPanel.add( leftSide );
		
		firstRow = new JPanel( new FlowLayout(FlowLayout.LEFT));
		leftSide.add( firstRow );
		
		theoremLabel = format(new JLabel("Theorem: "));
		firstRow.add( theoremLabel );
		
		theoremTextField = format(new JTextField(20));
		firstRow.add( theoremTextField );
		
		secondRow = new JPanel( new FlowLayout(FlowLayout.LEFT));
		leftSide.add( secondRow );
		
		axiomLabel = format(new JLabel("New Axiom: "));
		secondRow.add( axiomLabel );
		
		axiomTextField = format(new JTextField( 15 ));
		secondRow.add( axiomTextField );
		
		bump5px = new JLabel("");
		bump5px.setPreferredSize( new Dimension( 5, 20 ));
		secondRow.add( bump5px );
		
		addAxiomButton = format(new JButton("Add"));
		secondRow.add( addAxiomButton );
		
		thirdRow = new JPanel( new FlowLayout(FlowLayout.LEFT));
		leftSide.add( thirdRow );
		
		axiomsLabel = format(new JLabel("Axioms: "));
		thirdRow.add(axiomsLabel);
		
		axiomComboBox = new JComboBox();
		axiomComboBox.setPreferredSize( new Dimension(160, 30));
		thirdRow.add(axiomComboBox);
		
		removeAxiomButton = format( new JButton("Remove Axiom"));
		thirdRow.add(removeAxiomButton);
		
		rightSide = new JPanel(new FlowLayout(FlowLayout.CENTER));
		rightSide.setPreferredSize( new Dimension(180, 130) );
		rightSide.setBorder(BorderFactory.createMatteBorder(0, 2, 0, 0, Color.darkGray));

		topPanel.add( rightSide );
		
		resetButton = format( new JButton("Reset"));
		rightSide.add( resetButton );
		
		proveButton = format( new JButton("Prove"));
		rightSide.add( proveButton );
		
		String instructionText = "\nUse '" + CNFSentence.OR + "' to denote 'or.'"
				               + "\nUse '" + CNFSentence.AND + "' to denote 'and.'"
				               + "\nUse " + CNFSentence.NOT + " to negate a literal.";
		instructions = new JTextArea(instructionText);
		instructions.setEditable(false);
		instructions.setWrapStyleWord(true);
		instructions.setFont( new Font("arial", Font.PLAIN, 14));
		instructions.setOpaque(false);
		rightSide.add( instructions );
		
		displayText = new JTextArea();
		displayText.setEditable(false);
		displayText.setWrapStyleWord(true);
		displayText.setFont( defaultFont );
		displayText.setOpaque( false );
		
		scrollPane = new JScrollPane( displayText );
		scrollPane.getViewport().setBackground( Color.white );
		
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 1.0;
		c.weighty = 1.0;
		
		this.add( scrollPane, c );
	}
	
	private JTextField format( JTextField textField ) {
		textField.setFont( defaultFont );
		return textField;
	}
	
	private JLabel format( JLabel label ) {
		label.setFont( defaultFont );
		label.setPreferredSize( new Dimension( 100, 20 ));
		return label;
	}
	
	private JButton format( JButton button ) {
		button.addActionListener( this );
		return button;
	}

	//=== Helper Methods Related to Buttons ==================//

	@Override
	public void actionPerformed(ActionEvent e) {
		if( e.getSource() == addAxiomButton ) {
			addAxiom();
		}
		else if( e.getSource() == removeAxiomButton ) {
			removeAxiom();
		}
		else if( e.getSource() == resetButton ) {
			reset();
		}
		else if( e.getSource() == proveButton ) {
			prove();
		}
	}
	
	private void addAxiom() {
		if( !axiomTextField.getText().trim().equals("") ) {
			CNFClause newAxiom = new CNFClause(axiomTextField.getText());
			axiomComboBox.addItem( newAxiom );
			axiomTextField.setText("");
		}
	}
	
	private void removeAxiom() {
		if( axiomComboBox.getSelectedItem() != null ) {
			axioms.remove( axiomComboBox.getSelectedItem() );
			axiomComboBox.removeItem( axiomComboBox.getSelectedItem() );
		}
	}
	
	private void reset() {
		int confirm = JOptionPane.showConfirmDialog(
				this,
			    "Are you sure you want to reset?");
			 
		if( confirm == 0 ) { //if yes button was pressed
			proof = null;
			axioms.clear();
			axiomComboBox.removeAllItems();
			theoremTextField.setText("");
			axiomTextField.setText("");
			displayText.setText("");
			scrollPane.getViewport().setBackground( Color.white );
		}
	}
	
	private void prove() {
		if( !theoremTextField.getText().trim().equals("") 
				&& axiomComboBox.getItemCount() > 0 ) {
			setAxioms();
			proof = new Proof( this.axioms, new CNFSentence(theoremTextField.getText()));
			displayText.setText("");
			//until the proof is shown provable or not provable...
			while( proof.isProvable() == null ) {
				proof.nextStep();
				displayText.setText( displayText.getText() + proof.getInstruction() );
			}
			if( proof.isProvable().booleanValue() == true ) {
				scrollPane.getViewport().setBackground( lightGreen );
				JOptionPane.showMessageDialog(this, "The theorem is true.");
			}
			else {
				scrollPane.getViewport().setBackground( lightRed );
				JOptionPane.showMessageDialog(this, "The theorem cannot be proven true.");
			}
		}
		else {
			JOptionPane.showMessageDialog(this,"Please enter a theorem and add at least one axiom.");
		}
	}
	
	private void setAxioms(){
		axioms.clear();
		for( int i=0; i<axiomComboBox.getItemCount(); i++ ){
			axioms.add( (CNFClause)axiomComboBox.getItemAt(i));
		}
	}

	//=== Main Method ========================================//
	
	public static void main( String[] args ) {
		ProofGUI gui = new ProofGUI();
		gui.setVisible(true);
	}
}