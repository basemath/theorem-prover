package com.bogledesigns.theoremProver;

/*
 * @author: David Bogle
 * 
 * The purpose of this class is to create an object that can
 * represent a literal that has a name assigned to it and has
 * a true or false value.  It is intended to be used in
 * clauses that represent logical expressions.
 */

public class Literal implements Comparable<Literal>{
	
	//=== Instance Variables =================================//
	
	private String  name;
	private boolean value;
	
	//=== Constructors =======================================//
	
	public Literal( String name ){
		this.name  = name.trim().toLowerCase();
		this.value = true; //literal is true by default
	}
	
	public Literal( String name, boolean value ) {
		this( name );
		this.value = value;
	}
	
	//=== Observors / Accessors ==============================//
	
	public String getName() {
		return this.name;
	}
	
	public boolean getValue() {
		return this.value;
	}
	
	public String toString(){
		//if the value is false, this statement appends the
		//negation character specified in the CNFSentence class.
		return this.name + ( (this.value)? "" : CNFSentence.NOT );
	}
	
	public Literal negation() {
		return new Literal( this.name, !this.value );
	}
	
	@Override
	public int hashCode() {
		return this.name.hashCode() + ((this.value) ? 1:0);
	}
	
	//@return true if names and values are equal
	@Override
	public boolean equals( Object theOther ) {

		return ( theOther != null
				&& theOther.getClass().equals(Literal.class)
				&& this.name.equals(((Literal) theOther).getName()) 
				&& this.value == ((Literal) theOther).getValue());
	}
	
	//@return true if the names are equal but values not equal
	public boolean compliments( Literal theOther ) {
		
		return ( this.name.equals(theOther.getName()) 
				&& this.value != theOther.getValue());
	}
	
	@Override
	public int compareTo( Literal another ) {
		return this.name.compareTo( another.getName() );
	}
	
	//=== Transformers / Mutators ============================//
	
	public void negate() {
		this.value = !value;
	}
}