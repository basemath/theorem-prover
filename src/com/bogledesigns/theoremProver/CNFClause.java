package com.bogledesigns.theoremProver;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 * @author: David Bogle
 * 
 * The purpose of this class is to create an object that
 * represents a clause in Conjunctive Normal Form. Since
 * the clause is in this form, each literal is assumed
 * to be separated by an "or."
 */

public class CNFClause {
	
	//=== Instance Variables =================================//
	
	private List<Literal> literals;
	
	//=== Constructors =======================================//
	
	public CNFClause( String expression ) {
		//get the literals
		this.literals = literalsFromString(expression.trim().toLowerCase());
		//sort the literals for easy comparison between clauses
		Collections.sort( this.literals );
	}
	
	public CNFClause( List<Literal> literals ) {
		this.literals = literals;
		//sort the literals for easy comparison between clauses
		Collections.sort( this.literals );
	}
	
	//=== Observors / Accessors ==============================//
	
	public String toString() {
		return stringFromLiterals( literals );
	}
	
	public List<Literal> getLiterals() {
		return this.literals;
	}
	
	public boolean equals( Object theOther ) {
		boolean equals = true;

		if( theOther != null && theOther.getClass().equals(CNFClause.class) ) {
			if( this.literals.size() == ((CNFClause)theOther).getLiterals().size() ) {
				for( int i=0; i<literals.size(); i++ ) {
					// this does work because the literals in a clause
					// are sorted when the clause is constructed
					if( !literals.get(i).equals(((CNFClause)theOther).getLiterals().get(i))) {
						equals = false;//a literal was different
						break; //we know it's false and can stop looping now
					}
				}
			}
			else {
				equals = false;//the number of literals was not the same
			}
		}
		else equals = false;//theOther was null or not a clause
		
		return equals;
	}
	
	/*
	 * @return true if this clause can resolve with theOther clause
	 */
	public boolean resolvesWith( CNFClause theOther ) {
		boolean resolves = false;
		
		//look for a complimentary literal
		for( int i=0; i<literals.size(); i++ ) {
			for( int j=0; j<theOther.getLiterals().size(); j++ ) {
				if( literals.get(i).compliments(theOther.getLiterals().get(j))) {
					resolves = true;//we found a complimentary literal
					break;
				}
			}
		}
		
		return resolves;
	}
	
	public List<CNFClause> resolvents( CNFClause theOther ) {
		List<CNFClause> resolvents = new ArrayList<CNFClause>();
		
		//a list of points is used to store the i and j indexes
		//of the complimentary literals in both lists of literals
		List<Point> complimentaryCoordinates = new ArrayList<Point>();
		
		//find all the complimentary literals
		for( int i=0; i<literals.size(); i++ ) {
			for( int j=0; j<theOther.getLiterals().size(); j++ ) {
				if( literals.get(i).compliments(theOther.getLiterals().get(j))) {
					complimentaryCoordinates.add( new Point( i, j ) );
				}
			}
		}
		
		//create the resolvent clauses
		for( Point complimentaryCoordinate : complimentaryCoordinates ) {
			List<Literal> newVariables = new ArrayList<Literal>();
			//add every literal that isn't the complimentary literal from both clauses
			for( int i=0; i<this.literals.size(); i++ ) {
				if( i != complimentaryCoordinate.x )
					newVariables.add( literals.get(i) );
			}
			for( int j=0; j<theOther.getLiterals().size(); j++ ) {
				if( j != complimentaryCoordinate.y 
						&& !newVariables.contains(theOther.getLiterals().get(j)))
					newVariables.add( theOther.getLiterals().get(j) );
			}
			resolvents.add(new CNFClause( newVariables));
		}
		
		return resolvents;
	}
	
	//=== Transformers / Mutators ============================//
	
	public void add( Literal newLiteral ) {
		this.literals.add( newLiteral );
		Collections.sort( this.literals );
	}
	
	//=== Helper Methods =====================================//
	
	//this method ensures that the given string is surrounded with parentheses
	private String parentheses( String input ) {
		String returnString = input;
		
		if( !input.equals("") ) {
			if( input.charAt(0) != '(' )
				returnString = "(" + returnString;
			
			if( input.charAt(input.length()-1) != ')')
				returnString = returnString + ")";
		}
		
		return returnString;
	}
	
	//used in the constructor to get literals from a string
	private List<Literal> literalsFromString( String clause ) {
		
		List<Literal> list = new ArrayList<Literal>();
		
		for( int i=0; i<clause.length(); i++ ) {
			if( Character.isLetter(clause.charAt(i)) ){
				if( i+1<clause.length() && clause.charAt(i+1) == CNFSentence.NOT )
					list.add(new Literal(""+clause.charAt(i), false));
				else 
					list.add(new Literal(""+clause.charAt(i), true));
			}
		}
		
		return list;
	}
	
	//used by toString
	private String stringFromLiterals( List<Literal> list ) {
		String returnString = "";
		
		for( int i=0; i<list.size(); i++ ) {
			returnString += list.get(i);
			returnString += ( i+1 < list.size() ) ? " " + CNFSentence.OR + " ": "" ;
		}
		
		//if there's more than one literal, put parentheses around the clause
		if( list.size() > 1 )
			returnString = parentheses( returnString );
		
		return returnString;
	}
}