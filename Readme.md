This program will take a theorem and a set of axioms  
(in boolean algebra) and compute whether the theorem  
can be proven true. It also displays to the user the  
steps taken to reach the conclusion of the proof.

This was another project for my algorithms class. I
worked alone on this.
